# Root-Finding-Numerical-Analysis
## Project 1 for Numerical Analysis course
This project aims to implement 5 root finding methods.
### A. Bracketing Methods:
1. Bisection Method
2. Regula-Falsi Method
### B. Open Methods:
1. Fixed Point Method 
2. Newton-Raphson Method
3. Secant Method

### Contributors
- Fatema Moharam ID.6655
- Nourhan Waleed ID.6609
- Heba Elwazzan ID.6521

### How to run the program
1. install the following python libraries using `pip3 install library_name`:
`tkinter`, 
`sympy`, 
`pandas`, 
`pillow`, 
`ttkthemes`.
2. navigate to the project folder
3. run the following command: `py root_finding.py`
